package org.ws4d.authentication.test;

/* This is some sample code for a client authenticating with a ws4d:lightbulb device
 * - directly via PIN on commandline(s)
 * - indirectly via flickering and tapping, brokered by
 *   a UI device (e.g. smartphone) (here be magic)
 */

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Scanner;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.authorization.AuthorizationException;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.protocol.http.Base64Util;
import org.ws4d.java.dispatch.DefaultServiceReference;
import org.ws4d.java.dispatch.DeviceServiceRegistry;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_Helper;

public class SimpleAuthenticationClient extends DefaultClient {

	public static final String myID = "urn:uuid:a768a1dd-aff5-40c6-9f7c-89d29d261e6c";
	
	public static boolean toggleBulb(Device bulbDev) {
		DefaultServiceReference lightBulbServiceReference = (DefaultServiceReference) (bulbDev.getServiceReference(new URI("http://www.ws4d.org/LightBulbService"), SecurityKey.EMPTY_KEY));
		Service lightBulbService = null;
		try {
			lightBulbService = lightBulbServiceReference.getService();
		} catch (CommunicationException e) {
			System.err.println("Error! Could not find Light Bulb Service @ Device " + bulbDev.getEndpointReference().toString());
			return false;
		};
		
		Operation stateOperation = lightBulbService.getOperation(null, "LightBulbState", null, null);
		Operation switchOperation = lightBulbService.getOperation(null, "LightBulbSwitch", null, null);
		
		ParameterValue stateIn = stateOperation.createInputValue();
		ParameterValue stateOut = null;
		try {
			stateOut = stateOperation.invoke(stateIn, CredentialInfo.EMPTY_CREDENTIAL_INFO);
		} catch (AuthorizationException e) {
			e.printStackTrace();
			System.err.println("stateOperation: " + e.getMessage());
			return false;
		} catch (InvocationException e) {
			e.printStackTrace();
			System.err.println("stateOperation: " + e.getMessage());
			return false;
		} catch (CommunicationException e) {
			e.printStackTrace();
			System.err.println("stateOperation: " + e.getMessage());
			return false;
		}
		String state = ParameterValueManagement.getString(stateOut, "state");
		String newState;
		
		if (state.toLowerCase().equals("off"))
			newState = "on";
		else
			newState = "off";
		
		ParameterValue switchIn = switchOperation.createInputValue();
		ParameterValue switchOut = null;
		ParameterValueManagement.setString(switchIn, "state", newState);
		ParameterValueManagement.setString(switchIn, "id", myID);
		
		try {
			switchOut = switchOperation.invoke(switchIn, CredentialInfo.EMPTY_CREDENTIAL_INFO);
		} catch (AuthorizationException e) {
			e.printStackTrace();
			System.err.println("switchOperation: " + e.getMessage());
			return false;
		} catch (InvocationException e) {
			e.printStackTrace();
			System.err.println("switchOperation: " + e.getMessage());
			return false;
		} catch (CommunicationException e) {
			e.printStackTrace();
			System.err.println("switchOperation: " + e.getMessage());
			return false;
		}
		
		System.out.println("Successfully switched " + ParameterValueManagement.getString(switchOut, "state"));
		
		return true;
	}
	
	public static void main(String[] args) {
		if (args.length == 0) {
			System.out.println("run java -jar client uuid1 [uuid2]");
			System.out.println("    uuid1 - uuid of device to authenticate with");
			System.out.println("    uuid2 - optional; uuid of device for indirect authentication");
			return;
		}
		
		JMEDSFramework.start(null);
		
		ECC_DH_Helper mCryptoHelper = new ECC_DH_Helper();
		
		Log.setLogLevel(Log.DEBUG_LEVEL_WARN);
		
		/* TODO: In a *real* scenario, target as well as uiAuthDev are of course dynamically discovered */
		EndpointReference targetEpr = new EndpointReference(new AttributedURI(args[0]));
		DeviceReference targetDevRef = DeviceServiceRegistry.getDeviceReference(targetEpr, SecurityKey.EMPTY_KEY);
		
		EndpointReference smartPhoneEpr = null;
		DeviceReference smartPhoneDevRef = null;
		Boolean indirectAuthentication = false;
		
		System.out.print("Using ");
		
		if (args.length == 2) {
			smartPhoneEpr = new EndpointReference(new AttributedURI(args[1]));
			smartPhoneDevRef = DeviceServiceRegistry.getDeviceReference(smartPhoneEpr, SecurityKey.EMPTY_KEY);
			indirectAuthentication = true;
			System.out.print("in");
		}
		
		System.out.println("direct Authentication.");
		
		boolean fake = false;
		
		try {
			Device bulbDev = null;
			try {
				bulbDev = targetDevRef.getDevice();
			} catch (CommunicationException e1) {
				System.out.println("Did not find Bulb Device! Was looking at " + targetDevRef.toString() + "\n" + e1.getMessage() + "\nAssuming Fake Device");
				fake = true;
			}
			
			/* if not in fake mode, try to switch light bulb. If successful, quit. Otherwise authenticate */
			
			if (!fake) {
				if (toggleBulb(bulbDev)) {
					JMEDSFramework.stop();
					JMEDSFramework.kill();
					return;
				}
			}

			String authenticationType;
			
			Device theAuthDev = null; /* will point to bulb or smartphone */
			
			if (indirectAuthentication) {
				Device smartPhoneDev = null;
				try {
					smartPhoneDev = smartPhoneDevRef.getDevice();
				} catch (CommunicationException e) {
					System.out.println("Did not find smart phone! Was looking at " + smartPhoneDevRef.toString() + "\n" + e.getMessage());
					return;
				}
				theAuthDev = smartPhoneDev;
				authenticationType = "http://www.ws4d.org/authentication/ui/mechanisms#pin";
			} else {
				theAuthDev = bulbDev;
				authenticationType = "http://www.ws4d.org/authentication/ui/mechanisms#flicker";
			}
			
			DefaultServiceReference authenticationServiceReference = (DefaultServiceReference) (theAuthDev.getServiceReferences(new QNameSet(new QName("AuthenticatedEllipticCurveDiffieHellman", "http://www.ws4d.org")), SecurityKey.EMPTY_KEY)).next();
			
			if (authenticationServiceReference == null) {
				System.out.println("ERROR: No Service found @ " + theAuthDev.getEndpointReference().getAddress().toString());
				return;
			}
			
			Service authenticationService = null;
			try {
				authenticationService = authenticationServiceReference.getService();
			} catch (CommunicationException e1) {
				System.out.println("Did not find Authentication Service! Was looking at " + theAuthDev.getEndpointReference().toString() + "\n" + e1.getMessage());
				return;
			}
			
			/* Do the first request */
			
			Operation op1 = authenticationService.getOperation(null, "ECC_DH1", null, null);
			ParameterValue p1 = op1.createInputValue();
			
			ParameterValueManagement.setString(p1, "TokenType", "http://www.ws4d.org/wscompactsecurity/tokentypes#symmetric-key-token");
			ParameterValueManagement.setString(p1, "RequestType", "http://www.ws4d.org/AuthenticatedEllipticCurveDiffieHellman/ECC_DH1");
			ParameterValueManagement.setString(p1, "AppliesTo", targetEpr.getAddress().toString());
			mCryptoHelper.getParams().setTarget(targetEpr.getAddress().toString());
			ParameterValueManagement.setString(p1, "OnBehalfOf", myID);
			mCryptoHelper.getParams().setOrigin(myID);
			ParameterValueManagement.setString(p1, "AuthenticationType", authenticationType);
			mCryptoHelper.getParams().addOtherA(authenticationType);
//			ParameterValueManagement.setString(p1, "", "");
			
			ParameterValue r1 = null;
			try {
				r1 = op1.invoke(p1, CredentialInfo.EMPTY_CREDENTIAL_INFO);
			} catch (AuthorizationException e1) {
				System.out.println("Authorization Exception when invoking ECC_DH1");
				return;
			} catch (CommunicationException e1) {
				System.out.println("Invocation Exception when invoking ECC_DH1");
				return;
			}
			
			System.out.println("===================================\n" + r1.toString() + "\n=================================");
			
			String cName = ParameterValueManagement.getString(r1, "RequestedSecurityToken/auth-ecc-dh-curvename");
			
			try {
				mCryptoHelper.init(cName);
			} catch (NoSuchAlgorithmException | NoSuchProviderException
					| InvalidAlgorithmParameterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String sNonce = ParameterValueManagement.getString(r1, "RequestedSecurityToken/auth-ecc-dh-nonce");
			
			mCryptoHelper.getParams().setNonceB(Base64Util.decode(sNonce));
			mCryptoHelper.getParams().addOtherB(ParameterValueManagement.getString(r1, "AuthenticationType"));
			
			byte[] encodedPublicKey = Base64Util.decode(ParameterValueManagement.getString(r1, "RequestedSecurityToken/auth-ecc-dh-public-key"));
			
			mCryptoHelper.getParams().setDistortedPublicKey(ECC_DH_Helper.decodePublicKey(encodedPublicKey));
			
			mCryptoHelper.createNonce();
			
			System.out.println("NonceA: " + Base64Util.encodeBytes(mCryptoHelper.getParams().getNonceA()));
			System.out.println("NonceB: " + Base64Util.encodeBytes(mCryptoHelper.getParams().getNonceB()));
			
			/* End of first request */
			
			
			/*
			 * Here be magic: OOB Shared Secret is exchanged
			 */
			
			System.out.print("PIN: ");
			Scanner in = new Scanner(System.in);
			int intOOBss = in.nextInt();
			in.close();
			mCryptoHelper.getParams().setOOBSharedSecretAsInt(intOOBss);
			mCryptoHelper.OOBkeyFromInteger();
			mCryptoHelper.decryptPublicKey();
			
			/* Do the second request */
			
			Operation op2 = authenticationService.getOperation(null, "ECC_DH2", null, null);
			ParameterValue p2 = op2.createInputValue();
			
			try {
				mCryptoHelper.calculateSharedSecret();
			} catch (InvalidKeyException | NoSuchAlgorithmException
					| NoSuchProviderException e) {
				e.printStackTrace();
			}
			
			mCryptoHelper.calculateCMAC();
			
			ParameterValueManagement.setString(p2, "TokenType", "http://www.ws4d.org/wscompactsecurity/tokentypes#symmetric-key-token");
			ParameterValueManagement.setString(p2, "RequestType", "http://www.ws4d.org/AuthenticatedEllipticCurveDiffieHellman/ECC_DH2");
			ParameterValueManagement.setString(p2, "AppliesTo", mCryptoHelper.getParams().getTarget());
			ParameterValueManagement.setString(p2, "OnBehalfOf", mCryptoHelper.getParams().getOrigin());
			
			ParameterValueManagement.setString(p2, "RequestedSecurityToken/auth-ecc-dh-nonce", Base64Util.encodeBytes(mCryptoHelper.getParams().getNonceA()));
			ParameterValueManagement.setString(p2, "RequestedSecurityToken/auth-ecc-dh-public-key", Base64Util.encodeBytes(mCryptoHelper.getParams().getKeyPair().getPublic().getEncoded()));
			ParameterValueManagement.setString(p2, "RequestedSecurityToken/auth-ecc-dh-cmac", Base64Util.encodeBytes(mCryptoHelper.getParams().getCMAC()));
			
			ParameterValue r2;
			try {
				r2 = op2.invoke(p2, CredentialInfo.EMPTY_CREDENTIAL_INFO);
			} catch (AuthorizationException e1) {
				System.out.println("Authorization Exception when invoking ECC_DH2");
				return;
			} catch (CommunicationException e1) {
				System.out.println("Invocation Exception when invoking ECC_DH2");
				return;
			}
			
			System.out.println("===================================\n" + r2.toString() + "\n=================================");
			
			/* only thing left is to check Bob's CMAC and calculate shared secret */
			mCryptoHelper.getParams().setForeignCMAC(Base64Util.decode(ParameterValueManagement.getString(r2, "RequestSecurityTokenResponse/RequestedSecurityToken/auth-ecc-dh-cmac")));
			
			boolean success = false;
			
			if (mCryptoHelper.checkCMAC()) {
				System.out.println("Congratulations! Bob's CMAC passed!");
				mCryptoHelper.calculateMasterKey();
				System.out.println("Calculated Master Key:");
				byte[] k = mCryptoHelper.getParams().getMasterKey();
				for (int i = 0; i < k.length; i++) {
					System.out.printf(" %02x", k[i]);
				}
				System.out.println("\n\n");
				success = true;
			} else {
				System.out.println("ERROR!! Bob's CMAC did not pass the test!!");
			}
			
			if (success && !fake)
				toggleBulb(bulbDev);
			
		} catch (InvocationException e) {
			e.printStackTrace();
		}
	
		JMEDSFramework.stop();
		
		JMEDSFramework.kill();
		
		return;
		
	}

}
